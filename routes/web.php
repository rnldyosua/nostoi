<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home')->name('home');

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/home', 'HomeController@index');
});

Route::group(['prefix' => 'page'], function(){
	Route::get('contact', 'PageController@contact')->name('contact');
	Route::post('contact', 'PageController@contactPost');
});

Route::get('room/{id}', ['as' => 'room', 'uses' => 'RoomController@page']);

Route::group(['prefix' => 'siteplan'], function(){
	Route::get('search', 'SiteplanController@booking')->name('search');
	Route::post('booking', 'SiteplanController@bookingPost')->name('booking');
	Route::post('confirm', 'SiteplanController@confirm')->name('confirm');
	Route::post('complete', 'SiteplanController@complete')->name('complete');
	Route::get('room/{id?}', 'SiteplanController@room');
});

Route::group(['prefix' => 'session'], function(){
	Route::get('/', function(){
		dd(session()->all());	
	});

	Route::get('flush', function(){
		session()->flush();	
	});
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
