<section id="search-bar">
	<div class="container">
		<div class="logo">
			<a href="{{ url('/') }}"><img src="{{ url('assets/images/nostoi-black.png') }}" alt="Nostoi"></a>
		</div>	
		<div class="toggle">
			<span class="text">menu</span>
			<a href="#" class="icon"><span></span></a>
		</div>
		<div class="col-md-20 col-md-offset-2">
			<form action="{{ route('booking') }}" method="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="row">
					<div class="col-md-10">
						<div class="input-daterange">
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<input type="text" class="input-md form-control from" name="start" id="fromDate" placeholder="Arrival" required/>
										<span class="input-group-addon calendar">&nbsp;</span>
									</div>					
								</div>
								<div class="col-xs-12">
									<div class="input-group">
								    	<input type="text" class="input-md form-control datepicker to" name="end" id="toDate" placeholder="Departure" required/>	
								    	<span class="input-group-addon calendar">&nbsp;</span>
								    </div>				    	
								</div>	
							</div>
						</div>
						<!-- #date-picker -->	
					</div>
					<div class="col-md-10">
						<div class="row">
							<div class="col-xs-12">
								<div class="input-group">
									<select class="input-md form-control" id="select-room" name="room" required>	
										<option value="">Select Room</option>
										@foreach($rooms as $room)
											<option value="{{ $room->id }}">{{ $room->name }}</option>
										@endforeach
									</select>
									<span class="input-group-addon room">&nbsp;</span>
								</div>					
							</div>
							<div class="col-xs-12">
								<div class="input-group">
									<select class="input-md form-control" id="select-siteplan" name="siteplan" disabled required>	
										<option value="">Select Siteplan</option>
									</select>
									<span class="input-group-addon siteplan">&nbsp;</span>
								</div>	
							</div>	
						</div>
						<!-- /.row -->
					</div>
					<!-- /.col-md-10 -->
					<div class="col-md-4">
						<input type="submit" class="btn btn-md btn-default btn-book" value="Book!">
					</div>
					<!-- /.col-md-4 -->
				</div>
				<!-- /.row -->
			</form>			
		</div>
		<!-- /.col-md-24 -->
	</div>
	<!-- /.container -->
</section>
<!-- /#search-bar -->		
