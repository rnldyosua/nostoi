@extends('mail.layout')
@section('content')
<table cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td class="left" style="padding-bottom:20px; text-align:left;">                      
    </td>
  </tr>
  <tr>
    <td class="left" style="padding-bottom:40px; text-align:left;">
    Hi {{ $request->name }},<br><br>
    Thanks for contacting us!
	We will look over your message and we will get in touch with you shortly.	
    </td>
  </tr>
  <tr>
    <td>Name : {{ $request->name }}</td>
  </tr>
  <tr>
    <td>Message : {{ $request->message }}</td>
  </tr>
</table>
@endsection