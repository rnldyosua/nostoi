@extends('mail.layout')
@section('content')
<table cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td class="left" style="padding-bottom:20px; text-align:left;">                      
    </td>
  </tr>
  <tr>
    <td class="left" style="padding-bottom:40px; text-align:left;">
    Hi {{ $request->name }},<br><br>
    Your order no #{{ $booking->id }} has been successfully placed. We will check your order status and give you an update. 
    </td>
  </tr>
</table>

<table cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td>
      <b>Arrival</b>
    </td>
    <td>
      <b>Departure</b>
    </td>
    <td>
      <b>Room</b>
    </td>
    <td>
      <b>Siteplan</b>
    </td>
  </tr>
  <tr>
    <td class="border-bottom" height="5"></td>
    <td class="border-bottom" height="5"></td>
    <td class="border-bottom" height="5"></td>
    <td class="border-bottom" height="5"></td>
  </tr>
  <tr>
    <td style="padding-top:5px;">
      {{ session('booking')['start'] }}
    </td>
    <td style="padding-top:5px;">
      {{ session('booking')['end'] }}
    </td>
    <td style="padding-top:5px;" class="mobile">
      {{ session('booking')['siteplan']->room->name }}
    </td>
    <td style="padding-top:5px;" class="mobile">
      {{ session('booking')['siteplan']->name }}
    </td>
  </tr>
</table>
@endsection