<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ Voyager::setting('title') }} - {{ ucfirst(Route::getCurrentRoute()->getName()) }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="{{Voyager::setting('description')}}">
	<link rel="apple-touch-icon" sizes="57x57" href="/assets/fav/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/assets/fav/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/fav/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/assets/fav/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/fav/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/assets/fav/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/assets/fav/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/assets/fav/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/assets/fav/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/assets/fav/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/assets/fav/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/assets/fav/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/assets/fav/favicon-16x16.png">
	<link rel="manifest" href="/assets/fav/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/assets/fav/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<link rel="stylesheet" href="{{ url('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
	<link rel="stylesheet" href="{{ url('assets/bower_components/slick-carousel/slick/slick.css') }}">
	<link rel="stylesheet" href="{{ url('assets/bower_components/slick-carousel/slick/slick-theme.css') }}">
	<link rel="stylesheet" href="{{ url('assets/stylesheets/styles.css?v='.rand(0,999)) }}">
	<script src="{{ url('assets/bower_components/jquery/dist/jquery.min.js') }}" type="text/javascript" charset="utf-8"></script>
	<script src="{{ url('assets/bower_components/moment/min/moment.min.js') }}" type="text/javascript" charset="utf-8"></script>
	<script src="{{ url('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" type="text/javascript" charset="utf-8"></script>
	<script src="{{ url('assets/bower_components/slick-carousel/slick/slick.min.js') }}" type="text/javascript" charset="utf-8"></script>
	<script src="{{ url('assets/bower_components/jquery-sticky/jquery.sticky.js') }}" type="text/javascript" charset="utf-8"></script>
	<script>
		var APP_URL = "{!! url('/') !!}";
	</script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', '{{Voyager::setting('google_analytics_client_id')}}', 'auto');
	  ga('send', 'pageview');

	</script>
	<script>
		$(document).ready(function(){
			//Smooth Scroll
			$('a[href*="#"]:not([href="#"])').click(function() {
			    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			      	var target = $(this.hash);
			      	target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			      	if (target.length) {
			        	$('html, body').animate({
			          	scrollTop: (target.offset().top - 100)
			        	}, 1000);
			        	return false;
			    	}
				}
			});

			// Sticky Bar
			$('#search-bar').sticky();
			$('#search-bar').on('sticky-start', function(){
				$('header#main nav.main').css('position', 'absolute');
				$('#search-bar .logo, #search-bar .toggle').fadeIn(0);				
			})

			$('#search-bar').on('sticky-end', function(){
				$('header#main nav.main').css('position', 'fixed');
				$('#search-bar .logo, #search-bar .toggle').fadeOut(0);
			})
			// Date Picker Booking
			var today = new Date();			
			$('.input-daterange').datepicker({
				startDate: '0',
			    autoclose: true,			    
			    minDate: false,			    
			});

			// Toggle Menu
			$('.toggle .icon').on('click', function(event){
				event.preventDefault();
				$(this).toggleClass('active');
				$('.backdropmenu').fadeToggle(200);
				$('body').toggleClass('overflow-hidden');
			})

			$('.backdropmenu .menu a').on('click', function(){					
				$('.backdropmenu').fadeToggle(200);
				$('body').toggleClass('overflow-hidden');				
				$('.toggle .icon').removeClass('active');
			})

			// Facility Slick
			var facility_slide = $('.slide-1 .border'),
				facility_menu = $('.facilities .navigation ul li');
			facility_slide.slick({
				dots: false,
				arrows: true,
				autoplay: true,
				autoplaySpeed: 5000,				
				infinite: false,
				cssEase: 'linear'
			});

			facility_slide.on('afterChange', function(event, slick, currentSlide){
				facility_menu.removeClass('active');
				facility_menu.eq(currentSlide).addClass('active');
			})

			facility_menu.find('a').on('click', function(event){
				event.preventDefault();

				facility_slide.slick('slickGoTo', $(this).attr('data-slide'));
			})

			// Siteplan Deluxe Slick
			$('.siteplan-deluxe .siteplan-field').slick({
				arrows: true,
				dots: true,
				responsive: [
					{
						breakpoint: 768,
						settings: {
							dots: false
						}
					}
				]
			});
			// Drop Down Siteplan
			var ssplan = $('#select-siteplan');

			$('#select-room').on('change', function(){
				ssplan.attr('disabled', 'disabled');
				$.get(APP_URL+'/siteplan/room/'+$(this).val(), function(data){
					ssplan.removeAttr('disabled');
					var options = '<option value="">Select Siteplan</option>';
					data.forEach(function(siteplan){
						options += '<option value="'+siteplan.id+'">'+siteplan.name+'</option>';
					})
					ssplan.html(options);
				})				
			})
		})
	</script>
</head>
<body class="{{ Route::getCurrentRoute()->getName() }}">
	<header id="main">		
		<div class="backdropmenu">
			<div class="menu">
				<a href="{{ url('/') }}">Home</a>
				<a href="{{ url('/#rooms') }}">Rooms</a>
				<a href="{{ route('search') }}">Booking</a>
				<a href="http://instagram.com" target="_blank">Instagram</a>
				<a href="{{ route('contact') }}">Contact Us</a>
			</div>
			<!-- /.menu -->			
		</div>
		<!-- /.backdropmenu -->		
		<nav class="main">
			<div class="logo">
				<a href="{{ url('/') }}"><img src="@if(Route::getCurrentRoute()->getName() == 'home'){{ url('storage/'. Voyager::setting('logo')) }} @else {{ url('assets/images/nostoi-black.png') }}@endif" alt="Nostoi"></a>
			</div>	
			<div class="toggle">
				<span class="text">menu</span>
				<a href="#" class="icon"><span></span></a>
			</div>
		</nav>
		<!-- /.main -->	
		@yield('headerContent')		
	</header>
	<div class="container main">
		@yield('content')
	</div>
	<!-- /.container -->
	<footer class="main">
		<div class="container">
			<div class="col-md-10">
				<div class="col-xs-8">
					<a href="{{ url('/') }}"><img src="{{ url('assets/images/nostoi-logo.png') }}" alt="Nostoi" class="logo"></a>
				</div>
				<div class="col-xs-16">
					<ul class="navigation">
						<li><a href="{{ url('/') }}">Home</a></li>
						<li><a href="{{ url('/#rooms') }}">Rooms</a></li>
						<li><a href="{{ route('search') }}">Booking</a></li>
						<li><a href="{{ route('contact') }}">Contact Us</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-10">
				<div class="col-xs-12 blocks address">
					{!! Devitright::block(2)->content !!}
				</div>
				<!-- /.col-md-12 -->
				<div class="col-xs-12 blocks info">
					{!! Devitright::block(3)->content !!}
				</div>
				<!-- /.col-md-12 -->
			</div>
			<div class="col-md-4">
				<h5>STAY IN TOUCH</h5>
				<a href="#" target="_blank"><img src="{{ url('assets/images/facebook-footer.png') }}" alt="Facebook"></a>&nbsp;
				<a href="#" target="_blank"><img src="{{ url('assets/images/twitter-footer.png') }}" alt="Facebook"></a>
			</div>
		</div>
		<!-- /.container -->
		
	</footer>
</body>
</html>