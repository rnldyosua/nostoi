@extends('layouts.main')
@section('content')
	<div class="row border-white">
		<div class="col-md-12 map-field">	
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3966.4180250874656!2d106.82409235952473!3d-6.2084648818843835!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sid!2sid!4v1464325516540" height="400" frameborder="0" style="border:0; width: 100%; display: block;"  allowfullscreen></iframe>
		</div>
		<div class="col-md-12 form-field">
			<form action="{{ route('contact') }}" method="POST" class="col-md-18 col-md-offset-3">
				@if (count($errors) > 0)
				    <div class="alert alert-danger" style="margin-top: 20px;">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				@if(session()->has('contactSent'))
					<div class="alert alert-success" style="margin-top: 20px;">
						<b>{{ session('contactSent') }}</b>
					</div>
				@endif
				<h3><b>ASK US ANYTHING!</b></h3>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="input-group">
					<span class="input-group-addon">Name</span>
					<input type="text" class="input-md form-control" name="name"/>
				</div>
				<div class="input-group">
					<span class="input-group-addon">Email Address</span>
					<input type="email" class="input-md form-control" name="email"/>
				</div>
				<div class="input-group">
					<span class="input-group-addon">Message</span>
					<textarea name="message" class="input-md form-control" cols="30" rows="8"></textarea>
				</div>
				<input type="submit" class="btn btn-default btn-book">
			</form>
		</div>
	</div>
@endsection