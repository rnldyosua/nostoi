@extends('layouts.main')
@section('headerContent')
	<section id="main-header">
		<img src="{{ url('assets/images/main-header.jpg') }}" alt="Background" class="bg">
		<div class="headline">
			<img src="{{ url('assets/images/headline-nostoi.png') }}" alt="Headline">
		</div>
	</section>
	<!-- /#welcome-header -->
	@include('blocks.search')
@endsection
@section('content')	
	<div class="row">
		<div class="blocks home col-md-18 col-md-offset-3">		
			{!! Devitright::block(1)->content !!}
			
		</div>
		<!-- /.blocks home -->
	</div>	
	<div class="blocks rooms" id="rooms">
		<div class="row">
			<div class="	col-md-24">
				<span class="title">
					ROOMS SELECTIONS
				</span>	
			</div>			
			@foreach($rooms as $room)
				<div class="col-md-6 room">
					<div class="inner-border">
						@php
							$rImageInfo = pathinfo($room->image);
						@endphp
						<a href="{{ route('room', ['id' => $room->id]) }}"><img src="{{ url('storage/'.$rImageInfo['dirname'].'/'.$rImageInfo['filename'].'-cropped.'.$rImageInfo['extension']) }}" alt="{{ $room->name }}"></a>
						<h4><a href="{{ route('room', ['id' => $room->id]) }}">{{ $room->name }}</a></h4>							
					</div>					
				</div>
			@endforeach	
		</div>
		<!-- /.row -->			
	</div>
	<!-- /.rooms -->
	<div class="blocks facilities">
		<div class="row">
			<div class="col-md-24">
				<header class="navigation">
					<ul class="center-menu">
						@foreach($facilities as $key => $facility)
							<li @if($key == 0) class="active" @endif>
								<a href="void:(0)" data-slide="{{ $key }}">{{ $facility->name }}</a>
							</li>
						@endforeach
					</ul>
				</header>
				<!-- /.navigation -->
				<div class="slide-1">
					<div class="border">
						@foreach($facilities as $facility)
							<div class="facility">
								<div class="row">
									<div class="col-md-12 col-md-push-12">
										<img src="{{ url('storage/'.$facility->image) }}" alt="{{ $facility->name }}" class="image">
									</div>
									<div class="col-md-12 col-md-pull-12 content">
										<img src="{{ url('storage/'.$facility->image) }}" alt="{{ $facility->name }}" class="none">
										<img src="{{ url('storage/'.$facility->background) }}" class="background">
										<div class="inner">
											{!! $facility->description !!}	
										</div>
										<span class="open">
											{{ $facility->open }}
										</span>
									</div>
								</div>
								<!-- /.row -->							
							</div>
						@endforeach
					</div>
					<!-- /.border -->					
				</div>
				<!-- /.slide-1 -->
			</div>
			<!-- /.col-md-24 -->
			
		</div>
		<!-- /.row -->
		
	</div>
	<!-- /.facilities -->	
@endsection