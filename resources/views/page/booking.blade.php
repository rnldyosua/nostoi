@extends('layouts.main')
@section('content')	
	<div class="col-md-16 col-md-offset-4" style="margin-bottom: 30px;">
		{!! Devitright::block(4)->content !!}	
	</div>
	@if (count($errors) > 0)
	    <div class="alert alert-danger" style="margin-top: 20px;">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	@include('blocks.search')
@endsection