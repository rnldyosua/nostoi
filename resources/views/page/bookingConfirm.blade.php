@extends('layouts.main')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-14 col-md-offset-5 booking-wrapper">
				<h4><b>YOU HAVE ORDERED,</b></h4>
				<div class="booking-detail">
					<b>A {{ $siteplan->room->name }} - {{ $siteplan->name }} from {{ Request::input('start') }} - {{ Request::input('end') }}</b>	
				</div>
				<p>Please drop us your email and reachable contact number for further confirmation.<br>
				Our staff will contact you as soon as possible. Thank you!</p>
				<div class="row">
					<div class="col-md-14 col-md-offset-5 form-wrapper">
						<form action="{{ route('confirm') }}" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="input-group">
								<span class="input-group-addon">Name</span>
								<input type="text" class="input-md form-control" name="name" required/>
							</div>		
							<div class="input-group">
								<span class="input-group-addon">Email</span>
								<input type="email" class="input-md form-control" name="email" required/>
							</div>		
							<div class="input-group">
								<span class="input-group-addon">Mobile Phone</span>
								<input type="text" class="input-md form-control" name="mobile_phone"/>
							</div>		
							<div class="input-group">
								<span class="input-group-addon">Other Number</span>
								<input type="text" class="input-md form-control" name="other_number"/>
							</div>		
							<input type="submit" name="submit" value="Submit" class="btn btn-md btn-default btn-book">
						</form>
					</div>
				</div>
				
			</div>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
@endsection