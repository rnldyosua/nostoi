@extends('layouts.main')
@section('headerContent')
	@include('blocks.search')
	<div class="container">
		<div class="row">
			<div class="col-md-24">				
				<ul class="center-menu">
					@foreach($rooms as $val)
						<li @if($room->id == $val->id) class="active" @endif><a href="{{ route('room', ['id' => $val->id]) }}">{{ $val->name }}</a></li>
					@endforeach
				</ul>		
			</div>
		</div>
	</div>
	<!-- /.container -->
	<section id="main-header">
		<img src="{{ url('storage/'. $room->image) }}" alt="{{ $room->name }}" class="bg">
	</section>
	<!-- /#main-header -->
	<div class="container">
		<section class="room-detail">
			<div class="row">
				<div class="col-md-24">					
					<div class="row">
						<div class="col-md-8 room-available">
							<h5><b>{{ $room->available }}</b> Rooms available | <b>{{ count($room->siteplan) }}</b> Siteplans</h5>
						</div>
						<div class="col-md-8 name-field">
							<h3>{{ strtoupper($room->name) }}</h3>
						</div>
						<div class="col-md-8 area-field">
							<span>
								<b>{{ $room->available }}</b>
								Meter<sup>2</sup>
							</span>
						</div>		
					</div>
				</div>
			</div>
		</section>
		<!-- /.room-details -->
	</div>
@endsection
@section('content')		
	@yield('siteplan')
@endsection