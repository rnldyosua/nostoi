@extends('room.main')
@section('siteplan')
	<div class="row siteplan-twobedroom">
		<div class="col-md-24">
			@foreach($room->siteplan as $siteplan)
				<div class="row siteplan">
					<div class="col-md-12 siteplan-field">
						<img src="{{ url('storage/'.$siteplan->siteplan) }}" alt="Siteplan {{ $siteplan->name }}">	
						@if($siteplan->pdf)
							<a href="{{ url('storage/'.$siteplan->pdf) }}" class="pdf-field">DOWNLOAD PDF SITEPLAN</a>
						@endif
					</div>
					<div class="col-md-12">
						<div class="facilities-field">
							<div class="background-left"></div>
							{!! $room->facilities !!}
						</div>
						<div class="row rates">
							<div class="col-md-24 head">
								<div class="center">
									<b>Unit Rates</b>	
								</div>
							</div>
							<div class="col-xs-24 rates-field">
								<div class="bg">
									<div class="center">
										<span>{{ $room->night }} / Night</span> <span class="separator"></span> <span>{{ $room->month }} / Month</span>	
									</div>	
								</div>
							</div>					
						</div>
					</div>
				</div>
			@endforeach	
		</div>
@endsection