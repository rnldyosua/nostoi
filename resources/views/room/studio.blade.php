@extends('room.main')
@section('siteplan')
	<div class="row siteplan-studio">
		@foreach($room->siteplan as $siteplan)
			<div class="col-md-12 siteplan">
				<img src="{{ url('uploads/images/siteplans/'.$siteplan->photo) }}" alt="{{ $siteplan->name }}">				
				<h4 class="title-field"><b>{{ strtoupper($siteplan->name) }}</b> | {{ $room->area }}M<sup>2</sup></h4>
				<div class="row siteplan-description">
					<div class="col-md-12 siteplan-field">
						<img src="{{ url('storage/'.$siteplan->siteplan) }}" alt="Siteplan {{ $siteplan->name }}">
						@if($siteplan->pdf)
							<a href="{{ url('storage/'.$siteplan->pdf) }}" class="pdf-field">DOWNLOAD PDF SITEPLAN</a>
						@endif
					</div>
					<div class="col-md-12 facilities-field">
						{!! $room->facilities !!}
					</div>
				</div>
				<!-- /.row -->
				<div class="row rates">
					<div class="col-md-24 head">
						<div class="center">
							<b>Unit Rates</b>	
						</div>
					</div>
					<div class="col-xs-24 rates-field">
						<div class="bg">
							<div class="center">
								<span>{{ $room->night }} / Night</span> <span class="separator"></span> <span>{{ $room->month }} / Month</span>	
							</div>	
						</div>
					</div>					
				</div>
			</div>
		@endforeach
	</div>
	<!-- /.row -->
@endsection