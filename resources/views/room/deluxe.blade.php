@extends('room.main')
@section('siteplan')
	<div class="row siteplan-deluxe">
		<div class="col-md-6 col facilities-field">
			<b>FACILITIES</b>
			{!! $room->facilities !!}
		</div>
		<div class="col-md-12 col siteplan-field">
			@foreach($room->siteplan as $siteplan)
				<div class="image">
					<b>{{ $siteplan->name }}</b>
					<img src="{{ url('storage/'.$siteplan->siteplan) }}" alt="Siteplan {{ $siteplan->name }}">	
					@if($siteplan->pdf)
						<a href="{{ url('storage/'.$siteplan->pdf) }}" class="pdf-field">DOWNLOAD PDF SITEPLAN</a>
					@endif								
				</div>
			@endforeach
		</div>
		<div class="col-md-6 col rates-field">
			<b>UNIT RATES</b>
			<span class="price">{{ $room->night }} / Night</span>
			<span class="divider"></span>
			<span class="price">{{ $room->month }} / Month</span>
		</div>		
	</div>
	<!-- /.row -->
@endsection