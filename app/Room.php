<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
	protected $table = 'rooms';

    public function siteplan(){
		return $this->hasMany('App\Siteplan');
	}
}
