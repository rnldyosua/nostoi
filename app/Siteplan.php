<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siteplan extends Model
{
    protected $table = 'siteplans';

    public function room(){
    	return $this->belongsTo('App\Room');
    }

    public function roomId(){
    	return $this->belongsTo(Room::class, 'room_id', 'id');
    }
}
