<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
	protected $table = 'bookings';
	protected $fillable = array('name', 'email', 'mobile_phone', 'other_number', 'siteplan_id', 'room_id', 'start', 'end', 'ip_address');

    public function siteplan(){
    	return $this->belongsTo('App\Siteplan');
    }

    public function room(){
    	return $this->belongsTo('App\Room');
    }

    public function roomId(){
    	return $this->belongsTo(Room::class, 'room_id', 'id');
    }

    public function siteplanId(){
    	return $this->belongsTo(Siteplan::class, 'siteplan_id', 'id');
    }


}
