<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use TCG\Voyager\Facades\Voyager;
use App\Room;

class RoomDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Room::count();
        $string = $count == 1 ? 'room' : 'rooms';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-group',
            'title'  => "{$count} {$string}",
            'text'   => "You have {$count} {$string} in your database. Click on button below to view all rooms.",
            'button' => [
                'text' => 'View all rooms',
                'link' => route('voyager.rooms.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/01.png'),
        ]));
    }
}
