<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Mail;

use App\Room;

use App\Siteplan;

use App\Facility;

use App\Contact;

use Validator;

class PageController extends Controller
{	


	public function getSiteplans(){
		return Siteplan::orderBy('id', 'DESC')->get();    	
	}

	public function getFacilities(){
		return Facility::orderBy('created_at', 'ASC')->get();
	}

    public function home(){    	    	
    	$data['siteplans'] = $this->getSiteplans();
    	$data['facilities'] = $this->getFacilities();
    	return view('page.home')->with($data);
    }    

    public function contact(){
    	return view('page.contact');
    }

    public function contactPost(Request $request){
    	$validator = Validator::make($request->all(),[
    		'name' => 'required',
    		'email' => 'required',
    		'message' => 'required'
    	]);

    	if ($validator->fails()){
    		return redirect(route('contact'))->withErrors($validator)->withInput();
    	}else{
    		session()->flash('contactSent', 'Email Sent.');
    	}

    	$contact = Contact::create([
    		'name' => $request->name,
    		'email' => $request->email,
    		'message' => $request->message,
    		'ip_address' => $request->ip()
    	]);

    	Mail::send('mail.contactemail', ['request' => $request], function ($m) use ($request) {
            $m->from('noreply@nostoi.id', 'Nostoi');

            $m->to($request->email, $request->name)->subject('Contact Confirmation: Thank You');
        });

        Mail::send('mail.contactemail', ['request' => $request], function ($m) use ($request) {
            $m->from('noreply@nostoi.id', 'Nostoi');
            $m->to($request->email, $request->name)
            ->bcc(array('yosua.reinaldo@gmail.com', 'nostoiid@gmail.com'))
            ->subject('Contact Confirmation: Thank You, '.$request->name);
        });


    	return redirect(route('contact'));
    }
}
