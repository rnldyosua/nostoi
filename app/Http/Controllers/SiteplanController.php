<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Siteplan;

use App\Room;

use App\Booking;

use Mail;

use Validator;

class SiteplanController extends Controller
{
	public function detail($id){
		try {
			$data['siteplan'] = Siteplan::findOrFail($id);
		} catch (Exception $e) {
			return response()->view('errors.404');
		}		
	}

	public function room($id = null){		
		if($id == null){
			$siteplan = Siteplan::orderBy('name', 'ASC')->get();
		}else{
			$siteplan = Room::find($id)->siteplan()->get();
		}
		return response()->json($siteplan);
	}

    public function booking(){
        return view('page.booking');
    }

    public function bookingPost(Request $request){
        $validator = Validator::make($request->all(), [
            'room' => 'required',
            'siteplan' => 'required',
            'start' => 'required',
            'end' => 'required'
        ]);

        if($validator->fails()){
            return redirect(route('search'))->withErrors($validator)->withInput();
        }

    	try {
    		$data['siteplan'] = Siteplan::findOrFail($request->siteplan);              
    		session()->flash('booking',
    			[    				
    				'siteplan' => $data['siteplan'],                    
    				'start' => $request->start,
    				'end' => $request->end
    			]
    		);
    		return view('page.bookingConfirm', $data);
    	} catch (Exception $e) {
			return response()->view('errors.404');
    	}    	
    }

    public function confirm(Request $request){
    	
    	$booking = Booking::create([
    		'name' => $request->name,
    		'email' => $request->email,
    		'mobile_phone' => $request->mobile_phone,
    		'other_number' => $request->other_number,
    		'siteplan_id' => session('booking')['siteplan']->id,
    		'room_id' => session('booking')['siteplan']->room->id,
    		'start' => session('booking')['start'],
    		'end' => session('booking')['end'],
    		'ip_address' => $request->ip()
    	]);    	



        Mail::send('mail.confirmemail', ['request' => $request, 'booking' => $booking], function ($m) use ($request, $booking) {
            $m->from('noreply@nostoi.id', 'Nostoi');

            $m->to($request->email, $request->name)
            ->bcc(array('yosua.reinaldo@gmail.com', 'nostoiid@gmail.com'))
            ->subject('Booking Confirmation: #'.$booking->id.' Thank You ');
        });

        
    	return redirect(route('home'));    	
    }

    public function complete(Request $request){

    }
}
