<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Room;


class RoomController extends Controller
{

	public function page($id){
   		$data['room'] = Room::find($id);
		return view('room.'.$data['room']['original']['layout'], $data);		
	}
		
}
