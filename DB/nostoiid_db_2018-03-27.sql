# ************************************************************
# Sequel Pro SQL dump
# Version 4500
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.57-0ubuntu0.14.04.1)
# Database: nostoiid_db
# Generation Time: 2018-03-27 04:06:36 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table blocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blocks`;

CREATE TABLE `blocks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `blocks` WRITE;
/*!40000 ALTER TABLE `blocks` DISABLE KEYS */;

INSERT INTO `blocks` (`id`, `name`, `content`, `created_at`, `updated_at`)
VALUES
	(1,'About Home','<h3><strong>Nostoi</strong> (Greek: &Nu;?&sigma;&tau;&omicron;&iota;, Nostoi, &quot;Returns&quot;)</h3>\n\n<p>Nostoi means &ldquo;return home.&rdquo; To place Nostoi as a housing name refers to finding a sanctuary as people return home to find the comforts of their life in their own personal space. To once travel and make ends meet only with their pursuits and ideals only to find their own self and return in a place they can call home. Nostoi is a 6 level Apartment. There are 41 units available with different siteplans to be rented daily, monthly, or yearly.</p>\n','2016-05-26 17:24:55','2016-05-26 17:35:01'),
	(2,'Address Footer','<p>Jl. Sekitaran Kuningan No.8<br />\nJakarta Selatan<br />\nIndonesia</p>\n','2016-05-26 17:33:53','2016-05-26 17:34:48'),
	(3,'Info Footer','<p>info@nostoi.com<br />\ntel. +621 345678910<br />\nfax. +621 345678911</p>\n','2016-05-26 17:35:29','2016-05-26 17:35:29'),
	(4,'Search Block','<h2 style=\"text-align: center;\"><strong>Hello</strong></h2>\n\n<p style=\"text-align: center;\">Eget recusandae magna, laudantium dolorem maiores porttitor ipsum tristique, eget, incididunt? Penatibus elit blandit voluptate labore, dis! Sollicitudin tempora totam.</p>\n','2016-05-27 18:45:29','2016-05-27 18:45:29');

/*!40000 ALTER TABLE `blocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bookings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bookings`;

CREATE TABLE `bookings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `mobile_phone` varchar(255) NOT NULL DEFAULT '',
  `other_number` varchar(255) DEFAULT '',
  `siteplan_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `start` varchar(255) NOT NULL DEFAULT '',
  `end` varchar(255) NOT NULL DEFAULT '',
  `ip_address` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;

INSERT INTO `bookings` (`id`, `name`, `email`, `mobile_phone`, `other_number`, `siteplan_id`, `room_id`, `start`, `end`, `ip_address`, `created_at`, `updated_at`)
VALUES
	(1,'Reinaldo Yosua','yosua.reinaldo@gmail.com','123123123','123123',1,1,'05/25/2016','05/25/2016','192.168.33.1','2016-05-22 18:16:00','2016-05-22 18:16:00'),
	(2,'Yosua','yosua.reinaldo@gmail.com','0878788','12345',3,1,'05/27/2016','05/27/2016','192.168.33.1','2016-05-22 20:29:58','2016-05-22 20:29:58'),
	(3,'Reinaldo Yosua','yosua.reinaldo@gmail.com','08787878','12312321',1,1,'05/31/2016','05/31/2016','192.168.33.1','2016-05-26 18:01:26','2016-05-26 18:01:26'),
	(4,'Rei','yosua.reinaldo@gmail.com','08787878','12312321',1,1,'05/26/2016','05/26/2016','192.168.33.1','2016-05-26 18:10:48','2016-05-26 18:10:48'),
	(5,'Reinaldo Yosua','yosua.reinaldo@gmail.com','087878','123123',5,3,'05/26/2016','05/26/2016','192.168.33.1','2016-05-26 18:24:27','2016-05-26 18:24:27'),
	(6,'Klix Digital','reinaldo.yosua@klixdigital.com','0878788','12312321',3,2,'05/31/2016','05/31/2016','192.168.33.1','2016-05-26 18:34:24','2016-05-26 18:34:24'),
	(7,'Reinaldo Yosua','yosua.reinaldo@gmail.com','1231231','',3,2,'05/26/2016','05/26/2016','192.168.33.1','2016-05-26 18:45:30','2016-05-26 18:45:30'),
	(8,'Reinaldo Yosua','yosua.reinaldo@gmail.com','1232131','',2,1,'05/26/2016','05/31/2016','192.168.33.1','2016-05-26 18:48:16','2016-05-26 18:48:16'),
	(9,'Reinaldo Yosua','yosua.reinaldo@gmail.com','1232131','',2,1,'05/26/2016','05/31/2016','192.168.33.1','2016-05-26 18:49:20','2016-05-26 18:49:20'),
	(10,'Reinaldo Yosua','yosua.reinaldo@gmail.com','1232131','',2,1,'05/26/2016','05/31/2016','192.168.33.1','2016-05-26 18:49:34','2016-05-26 18:49:34'),
	(11,'Reinaldo Yosua','yosua.reinaldo@gmail.com','12312312','1231',6,3,'05/31/2016','06/02/2016','192.168.33.1','2016-05-26 19:21:40','2016-05-26 19:21:40'),
	(12,'Reinaldo Yosua','yosua.reinaldo@gmail.com','0878788','123123',5,3,'06/02/2016','06/02/2016','103.26.101.154','2016-05-27 19:15:14','2016-05-27 19:15:14'),
	(13,'Reinaldo Yosua','yosua.reinaldo@gmail.com','87888761005','22428',1,1,'05/30/2016','05/31/2016','115.178.211.31','2016-05-28 21:10:52','2016-05-28 21:10:52'),
	(14,'Reinaldo Yosua','yosua.reinaldo@gmail.com','08788788','08787878',3,2,'07/29/2016','07/29/2016','103.26.101.154','2016-07-19 18:54:34','2016-07-19 18:54:34'),
	(15,'Reinaldo Saroinsong','yosua.reinaldo@gmail.com','+6287888761001','',3,2,'10/25/2016','10/27/2016','103.26.101.154','2016-10-11 18:38:02','2016-10-11 18:38:02'),
	(16,'Test','reinaldo.yosua@klixdigital.com','08788787878','1231231',3,2,'09/04/2017','09/05/2017','103.26.101.154','2017-09-04 06:32:00','2017-09-04 06:32:00'),
	(17,'Reinaldo Yosua','yosua.reinaldo@gmail.com','1231312','123131',1,1,'09/07/2017','09/09/2017','192.168.33.1','2017-09-07 11:17:33','2017-09-07 11:17:33'),
	(18,'Reinaldo Yosua','yosua.reinaldo@gmail.com','1231231231','82312312',3,2,'09/18/2017','09/20/2017','192.168.33.1','2017-09-07 11:19:15','2017-09-07 11:19:15'),
	(19,'Reinaldo Yosua','yosua.reinaldo@gmail.com','8231231',NULL,3,2,'09/26/2017','09/27/2017','192.168.33.1','2017-09-07 11:20:27','2017-09-07 11:20:27'),
	(20,'Reinaldo Yosua','reinaldo.yosua@klixdigital.com','80878178231',NULL,3,2,'09/19/2017','09/20/2017','192.168.33.1','2017-09-07 11:24:33','2017-09-07 11:24:33'),
	(21,'Reinaldo Yosua','reinaldo.yosua@klixdigital.com','231231312','123123123',3,2,'09/13/2017','09/13/2017','103.26.101.154','2017-09-08 06:07:35','2017-09-08 06:07:35');

/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`)
VALUES
	(1,NULL,1,'Category 1','category-1','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(2,NULL,1,'Category 2','category-2','2017-09-05 10:11:44','2017-09-05 10:11:44');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table contacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE `contacts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `ip_address` varchar(255) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;

INSERT INTO `contacts` (`id`, `name`, `email`, `message`, `ip_address`, `created_at`, `updated_at`)
VALUES
	(1,'Reinaldo Yosua','yosua.reinaldo@gmail.com','Teeest!','192.168.33.1','2016-05-27 06:04:08','2016-05-27 06:04:08'),
	(2,'Reinaldo Yosua','yosua.reinaldo@gmail.com','Test 123','192.168.33.1','2016-05-27 07:05:51','2016-05-27 07:05:51'),
	(3,'Reinaldo Yosua','yosua.reinaldo@gmail.com','Rererer','192.168.33.1','2016-05-27 07:11:39','2016-05-27 07:11:39'),
	(4,'Reinaldo Yosua','yosua.reinaldo@gmail.com','test email','192.168.33.1','2017-09-07 10:45:30','2017-09-07 10:45:30'),
	(5,'Reinaldo Yosua','yosua.reinaldo@gmail.com','Test email boz','192.168.33.1','2017-09-07 10:52:38','2017-09-07 10:52:38'),
	(6,'Reinaldo Yosua','yosua.reinaldo@gmail.com','jjkjhhjhjhj','192.168.33.1','2017-09-07 10:53:37','2017-09-07 10:53:37'),
	(7,'Reinaldo Yosua','yosua.reinaldo@gmail.com','test bzzz','192.168.33.1','2017-09-07 10:56:12','2017-09-07 10:56:12'),
	(8,'Reinaldo Yosua','yosua.reinaldo@gmail.com','HALOOO','192.168.33.1','2017-09-07 10:59:45','2017-09-07 10:59:45'),
	(9,'Reinaldo Yosua','yosua.reinaldo@gmail.com','asd ada dsa','192.168.33.1','2017-09-07 11:00:19','2017-09-07 11:00:19'),
	(10,'Reinaldo Yosua','yosua.reinaldo@gmail.com','test s','192.168.33.1','2017-09-07 11:01:31','2017-09-07 11:01:31'),
	(11,'Reinaldo Yosua','reinaldo.yosua@klixdigital.com','Suspendisse sed alias pharetra penatibus repudiandae, mauris distinctio rutrum phasellus pharetra occaecati senectus parturient, cupiditate. Congue accumsan quasi litora facilis.','192.168.33.1','2017-09-07 11:27:56','2017-09-07 11:27:56');

/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table data_rows
# ------------------------------------------------------------

DROP TABLE IF EXISTS `data_rows`;

CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`)
VALUES
	(1,1,'id','number','ID',1,0,0,0,0,0,'',1),
	(2,1,'author_id','text','Author',1,0,1,1,0,1,'',2),
	(3,1,'category_id','text','Category',1,0,1,1,1,0,'',3),
	(4,1,'title','text','Title',1,1,1,1,1,1,'',4),
	(5,1,'excerpt','text_area','excerpt',1,0,1,1,1,1,'',5),
	(6,1,'body','rich_text_box','Body',1,0,1,1,1,1,'',6),
	(7,1,'image','image','Post Image',0,1,1,1,1,1,'\n{\n    \"resize\": {\n        \"width\": \"1000\",\n        \"height\": \"null\"\n    },\n    \"quality\": \"70%\",\n    \"upsize\": true,\n    \"thumbnails\": [\n        {\n            \"name\": \"medium\",\n            \"scale\": \"50%\"\n        },\n        {\n            \"name\": \"small\",\n            \"scale\": \"25%\"\n        },\n        {\n            \"name\": \"cropped\",\n            \"crop\": {\n                \"width\": \"300\",\n                \"height\": \"250\"\n            }\n        }\n    ]\n}',7),
	(8,1,'slug','text','slug',1,0,1,1,1,1,'\n{\n    \"slugify\": {\n        \"origin\": \"title\",\n        \"forceUpdate\": true\n    }\n}',8),
	(9,1,'meta_description','text_area','meta_description',1,0,1,1,1,1,'',9),
	(10,1,'meta_keywords','text_area','meta_keywords',1,0,1,1,1,1,'',10),
	(11,1,'status','select_dropdown','status',1,1,1,1,1,1,'\n{\n    \"default\": \"DRAFT\",\n    \"options\": {\n        \"PUBLISHED\": \"published\",\n        \"DRAFT\": \"draft\",\n        \"PENDING\": \"pending\"\n    }\n}',11),
	(12,1,'created_at','timestamp','created_at',0,1,1,0,0,0,'',12),
	(13,1,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',13),
	(14,2,'id','number','id',1,0,0,0,0,0,'',1),
	(15,2,'author_id','text','author_id',1,0,0,0,0,0,'',2),
	(16,2,'title','text','title',1,1,1,1,1,1,'',3),
	(17,2,'excerpt','text_area','excerpt',1,0,1,1,1,1,'',4),
	(18,2,'body','rich_text_box','body',1,0,1,1,1,1,'',5),
	(19,2,'slug','text','slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"}}',6),
	(20,2,'meta_description','text','meta_description',1,0,1,1,1,1,'',7),
	(21,2,'meta_keywords','text','meta_keywords',1,0,1,1,1,1,'',8),
	(22,2,'status','select_dropdown','status',1,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}',9),
	(23,2,'created_at','timestamp','created_at',1,1,1,0,0,0,'',10),
	(24,2,'updated_at','timestamp','updated_at',1,0,0,0,0,0,'',11),
	(25,2,'image','image','image',0,1,1,1,1,1,'',12),
	(26,3,'id','number','id',1,0,0,0,0,0,'',1),
	(27,3,'name','text','name',1,1,1,1,1,1,'',2),
	(28,3,'email','text','email',1,1,1,1,1,1,'',3),
	(29,3,'password','password','password',1,0,0,1,1,0,'',4),
	(30,3,'remember_token','text','remember_token',0,0,0,0,0,0,'',5),
	(31,3,'created_at','timestamp','created_at',0,1,1,0,0,0,'',6),
	(32,3,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',7),
	(33,3,'avatar','image','avatar',0,1,1,1,1,1,'',8),
	(34,5,'id','number','id',1,0,0,0,0,0,'',1),
	(35,5,'name','text','name',1,1,1,1,1,1,'',2),
	(36,5,'created_at','timestamp','created_at',0,0,0,0,0,0,'',3),
	(37,5,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',4),
	(38,4,'id','number','id',1,0,0,0,0,0,'',1),
	(39,4,'parent_id','select_dropdown','parent_id',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),
	(40,4,'order','text','order',1,1,1,1,1,1,'{\"default\":1}',3),
	(41,4,'name','text','name',1,1,1,1,1,1,'',4),
	(42,4,'slug','text','slug',1,1,1,1,1,1,'',5),
	(43,4,'created_at','timestamp','created_at',0,0,1,0,0,0,'',6),
	(44,4,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',7),
	(45,6,'id','number','id',1,0,0,0,0,0,'',1),
	(46,6,'name','text','Name',1,1,1,1,1,1,'',2),
	(47,6,'created_at','timestamp','created_at',0,0,0,0,0,0,'',3),
	(48,6,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',4),
	(49,6,'display_name','text','Display Name',1,1,1,1,1,1,'',5),
	(50,1,'seo_title','text','seo_title',0,1,1,1,1,1,'',14),
	(51,1,'featured','checkbox','featured',1,1,1,1,1,1,'',15),
	(52,3,'role_id','text','role_id',1,1,1,1,1,1,'',9),
	(58,9,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),
	(59,9,'name','text','Name',0,1,1,1,1,1,NULL,2),
	(60,9,'content','rich_text_box','Content',0,1,1,1,1,1,NULL,3),
	(61,9,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,4),
	(62,9,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,5),
	(63,10,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),
	(64,10,'name','text','Name',1,1,1,0,0,0,NULL,2),
	(65,10,'email','text','Email',1,1,1,0,0,0,NULL,3),
	(66,10,'mobile_phone','text','Mobile Phone',1,1,1,0,0,0,NULL,4),
	(67,10,'other_number','text','Other Number',0,1,1,0,0,0,NULL,5),
	(68,10,'siteplan_id','select_dropdown','Siteplan Name',1,1,1,0,0,0,'{\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',6),
	(69,10,'room_id','select_dropdown','Room Name',1,1,1,0,0,0,'{\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',7),
	(70,10,'start','text','Start',1,1,1,0,0,0,NULL,8),
	(71,10,'end','checkbox','End',1,1,1,0,0,0,NULL,9),
	(72,10,'ip_address','text','Ip Address',0,1,1,0,0,0,NULL,10),
	(73,10,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,11),
	(74,10,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,12),
	(75,11,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),
	(76,11,'name','text','Name',1,1,1,1,1,1,NULL,2),
	(77,11,'facilities','rich_text_box','Facilities',0,1,1,1,1,1,NULL,3),
	(78,11,'image','image','Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1140\",\"height\":\"null\"},\"quality\":\"80%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"220\"}}]}',4),
	(79,11,'available','text','Available',0,1,1,1,1,1,NULL,5),
	(80,11,'area','text','Area',0,1,1,1,1,1,NULL,6),
	(81,11,'layout','select_dropdown','Layout',0,1,1,1,1,1,'{\"default\":\"deluxe\",\"options\":{\"deluxe\":\"Deluxe\",\"penthouse\":\"Penthouse\",\"twobedroom\":\"Two Bed Room\",\"studio\":\"Studio\"}}',7),
	(82,11,'night','text','Night',0,1,1,1,1,1,NULL,8),
	(83,11,'month','text','Month',0,1,1,1,1,1,NULL,9),
	(84,11,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,10),
	(85,11,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,11),
	(86,12,'id','text','Id',1,0,0,0,0,0,NULL,1),
	(87,12,'room_id','select_dropdown','Room  Name',1,1,1,1,1,1,'{\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),
	(88,12,'name','text','Name',1,1,1,1,1,1,NULL,3),
	(89,12,'siteplan','image','Siteplan',0,1,1,1,1,1,'{\"quality\":\"100%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"cropped\",\"crop\":{\"width\":\"600\",\"height\":\"600\"}}]}',4),
	(90,12,'photo','image','Photo',0,1,1,1,1,1,'{\"quality\":\"100%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"square\",\"crop\":{\"width\":\"700\",\"height\":\"700\"}},{\"name\":\"cropped\",\"crop\":{\"width\":\"600\",\"height\":\"300\"}}]}',5),
	(91,12,'pdf','file','Pdf',0,1,1,1,1,1,NULL,6),
	(92,12,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,7),
	(93,12,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,8),
	(112,16,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),
	(113,16,'name','text','Name',0,1,1,1,1,1,NULL,2),
	(114,16,'description','rich_text_box','Description',0,1,1,1,1,1,NULL,3),
	(115,16,'open','text','Open',0,1,1,1,1,1,NULL,4),
	(116,16,'image','image','Image',0,1,1,1,1,1,NULL,5),
	(117,16,'background','image','Background',0,1,1,1,1,1,NULL,6),
	(118,16,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,7),
	(119,16,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,8),
	(120,17,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),
	(121,17,'name','text','Name',1,1,1,0,0,0,NULL,2),
	(122,17,'email','text','Email',1,1,1,0,0,0,NULL,3),
	(123,17,'message','text_area','Message',1,1,1,0,0,0,NULL,4),
	(124,17,'ip_address','text','Ip Address',1,1,1,0,0,1,NULL,5),
	(125,17,'created_at','timestamp','Created At',0,1,0,0,0,1,NULL,6),
	(126,17,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7);

/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table data_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `data_types`;

CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `controller`, `description`, `generate_permissions`, `server_side`, `created_at`, `updated_at`)
VALUES
	(1,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post','','',1,0,'2017-09-05 10:11:43','2017-09-05 10:11:43'),
	(2,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page','','',1,0,'2017-09-05 10:11:43','2017-09-05 10:11:43'),
	(3,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','','',1,0,'2017-09-05 10:11:43','2017-09-05 10:11:43'),
	(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category','','',1,0,'2017-09-05 10:11:43','2017-09-05 10:11:43'),
	(5,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu','','',1,0,'2017-09-05 10:11:43','2017-09-05 10:11:43'),
	(6,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role','','',1,0,'2017-09-05 10:11:43','2017-09-05 10:11:43'),
	(9,'blocks','blocks','Block','Blocks','voyager-list','App\\Block',NULL,NULL,1,0,'2017-09-05 10:26:25','2017-09-06 08:00:49'),
	(10,'bookings','bookings','Booking','Bookings','voyager-','App\\Booking',NULL,NULL,1,0,'2017-09-05 10:35:09','2017-09-06 09:47:22'),
	(11,'rooms','rooms','Room','Rooms','voyager-company','App\\Room',NULL,NULL,1,0,'2017-09-06 07:56:00','2017-09-06 07:56:00'),
	(12,'siteplans','siteplans','Siteplan','Siteplans','voyager-pie-chart','App\\Siteplan',NULL,NULL,1,0,'2017-09-06 08:10:05','2017-09-06 08:10:05'),
	(16,'facilities','facilities','Facility','Facilities',NULL,'App\\Facility',NULL,NULL,1,0,'2017-09-06 08:56:55','2017-09-06 08:56:55'),
	(17,'contacts','contacts','Contact','Contacts','voyager-mail','App\\Contact',NULL,NULL,1,0,'2017-09-06 10:25:49','2017-09-06 10:29:15');

/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table facilities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `facilities`;

CREATE TABLE `facilities` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `description` text,
  `open` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `background` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `facilities` WRITE;
/*!40000 ALTER TABLE `facilities` DISABLE KEYS */;

INSERT INTO `facilities` (`id`, `name`, `description`, `open`, `image`, `background`, `created_at`, `updated_at`)
VALUES
	(1,'Restaurant','<p><strong>Restaurant</strong></p>\r\n<p>Consequuntur. Lobortis sodales neque aliquid turpis commodi assumenda ipsa facilisi turpis dapibus? Reprehenderit. Leo iaculis, consectetuer, adipisicing totam metus condimentum.</p>','OPEN DAILY | 08 AM - 09 AM','facilities/September2017/yhZRgZZeE5Q6kwLJxxFG.jpg','facilities/September2017/yVoj5V8rzfRR8zAGzs1u.jpg','2016-05-21 00:13:00','2017-09-08 06:26:17'),
	(2,'Rooftop','<p><strong>Restaurant</strong></p>\r\n<p>Consequuntur. Lobortis sodales neque aliquid turpis commodi assumenda ipsa facilisi turpis dapibus? Reprehenderit. Leo iaculis, consectetuer, adipisicing totam metus condimentum.</p>','OPEN DAILY | 08 AM - 09 AM','facilities/September2017/gR74mQGBQvtuVVZv5vXn.jpg','facilities/September2017/Ta4Y1kHMWDDlhkQ0Crk2.jpg','2016-05-21 00:51:00','2017-09-08 06:25:46'),
	(3,'Bakery','<p>Erat eum elit per leo erat aliquam urna distinctio. Accusamus diam ratione dolores nulla platea nostrum! Habitasse nihil incidunt fames.</p>','OPEN DAILY | 08 AM - 09 AM','facilities/September2017/PmbTqioIwTRMnnTObuLs.jpg','facilities/September2017/KgidRKtNtk9OGWxPq0Sk.jpg','2016-07-19 16:51:00','2017-09-08 06:25:27');

/*!40000 ALTER TABLE `facilities` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menu_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu_items`;

CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`)
VALUES
	(1,1,'Dashboard','/admin','_self','voyager-boat',NULL,NULL,1,'2017-09-05 10:11:44','2017-09-05 10:11:44',NULL,NULL),
	(2,1,'Media','/admin/media','_self','voyager-images',NULL,NULL,8,'2017-09-05 10:11:44','2017-09-06 10:28:11',NULL,NULL),
	(3,1,'Posts','/admin/posts','_self','voyager-news',NULL,NULL,9,'2017-09-05 10:11:44','2017-09-06 10:28:11',NULL,NULL),
	(4,1,'Users','/admin/users','_self','voyager-person',NULL,NULL,11,'2017-09-05 10:11:44','2017-09-06 10:28:11',NULL,NULL),
	(5,1,'Categories','/admin/categories','_self','voyager-categories',NULL,NULL,13,'2017-09-05 10:11:44','2017-09-06 10:28:11',NULL,NULL),
	(6,1,'Pages','/admin/pages','_self','voyager-file-text',NULL,NULL,10,'2017-09-05 10:11:44','2017-09-06 10:28:11',NULL,NULL),
	(7,1,'Roles','/admin/roles','_self','voyager-lock',NULL,NULL,12,'2017-09-05 10:11:44','2017-09-06 10:28:11',NULL,NULL),
	(8,1,'Tools','','_self','voyager-tools',NULL,NULL,14,'2017-09-05 10:11:44','2017-09-06 10:28:11',NULL,NULL),
	(9,1,'Menu Builder','/admin/menus','_self','voyager-list',NULL,8,1,'2017-09-05 10:11:44','2017-09-05 10:53:01',NULL,NULL),
	(10,1,'Database','/admin/database','_self','voyager-data',NULL,8,2,'2017-09-05 10:11:44','2017-09-05 10:53:01',NULL,NULL),
	(11,1,'Settings','/admin/settings','_self','voyager-settings',NULL,NULL,15,'2017-09-05 10:11:44','2017-09-06 10:28:11',NULL,NULL),
	(12,1,'Blocks','/admin/blocks','_self','voyager-list','#000000',NULL,6,'2017-09-05 10:52:53','2017-09-06 08:14:03',NULL,''),
	(13,1,'Bookings','admin/bookings','_self','voyager-book','#000000',NULL,5,'2017-09-06 07:53:12','2017-09-06 08:14:03',NULL,''),
	(14,1,'Rooms','admin/rooms','_self','voyager-company','#000000',NULL,2,'2017-09-06 07:56:27','2017-09-06 08:11:27',NULL,''),
	(16,1,'Siteplans','admin/siteplans','_self','voyager-pie-chart','#000000',NULL,3,'2017-09-06 08:10:38','2017-09-06 08:11:20',NULL,''),
	(17,1,'Facilities','admin/facilities','_self','voyager-campfire','#000000',NULL,4,'2017-09-06 08:13:54','2017-09-06 08:14:03',NULL,''),
	(18,1,'Contacts','admin/contacts','_self','voyager-mail','#000000',NULL,7,'2017-09-06 10:28:01','2017-09-06 10:28:58',NULL,'');

/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'admin','2017-09-05 10:11:44','2017-09-05 10:11:44');

/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2016_01_01_000000_add_voyager_user_fields',1),
	(4,'2016_01_01_000000_create_data_types_table',1),
	(5,'2016_01_01_000000_create_pages_table',1),
	(6,'2016_01_01_000000_create_posts_table',1),
	(7,'2016_02_15_204651_create_categories_table',1),
	(8,'2016_05_19_173453_create_menu_table',1),
	(9,'2016_10_21_190000_create_roles_table',1),
	(10,'2016_10_21_190000_create_settings_table',1),
	(11,'2016_11_30_135954_create_permission_table',1),
	(12,'2016_11_30_141208_create_permission_role_table',1),
	(13,'2016_12_26_201236_data_types__add__server_side',1),
	(14,'2017_01_13_000000_add_route_to_menu_items_table',1),
	(15,'2017_01_14_005015_create_translations_table',1),
	(16,'2017_01_15_000000_add_permission_group_id_to_permissions_table',1),
	(17,'2017_01_15_000000_create_permission_groups_table',1),
	(18,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),
	(19,'2017_03_06_000000_add_controller_to_data_types_table',1),
	(20,'2017_04_21_000000_add_order_to_data_rows_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`)
VALUES
	(1,0,'Hello World','Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.','<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','pages/AAgCCnqHfLlRub9syUdw.jpg','hello-world','Yar Meta Description','Keyword1, Keyword2','ACTIVE','2017-09-05 10:11:44','2017-09-05 10:11:44');

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table permission_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permission_groups`;

CREATE TABLE `permission_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table permission_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;

INSERT INTO `permission_role` (`permission_id`, `role_id`)
VALUES
	(1,1),
	(1,3),
	(2,1),
	(3,1),
	(3,3),
	(4,1),
	(4,3),
	(5,1),
	(6,1),
	(7,1),
	(8,1),
	(9,1),
	(10,1),
	(11,1),
	(12,1),
	(13,1),
	(14,1),
	(15,1),
	(16,1),
	(17,1),
	(18,1),
	(19,1),
	(20,1),
	(21,1),
	(22,1),
	(23,1),
	(24,1),
	(25,1),
	(26,1),
	(27,1),
	(28,1),
	(29,1),
	(30,1),
	(31,1),
	(32,1),
	(33,1),
	(34,1),
	(40,1),
	(40,3),
	(41,1),
	(41,3),
	(42,1),
	(42,3),
	(43,1),
	(43,3),
	(44,1),
	(44,3),
	(45,1),
	(45,3),
	(46,1),
	(46,3),
	(50,1),
	(50,3),
	(51,1),
	(51,3),
	(52,1),
	(52,3),
	(53,1),
	(53,3),
	(54,1),
	(54,3),
	(55,1),
	(55,3),
	(56,1),
	(56,3),
	(57,1),
	(57,3),
	(58,1),
	(58,3),
	(59,1),
	(59,3),
	(75,1),
	(75,3),
	(76,1),
	(76,3),
	(77,1),
	(77,3),
	(78,1),
	(78,3),
	(79,1),
	(79,3),
	(80,1),
	(80,3),
	(81,1),
	(81,3);

/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `permission_group_id`)
VALUES
	(1,'browse_admin',NULL,'2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(2,'browse_database',NULL,'2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(3,'browse_media',NULL,'2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(4,'browse_settings',NULL,'2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(5,'browse_menus','menus','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(6,'read_menus','menus','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(7,'edit_menus','menus','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(8,'add_menus','menus','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(9,'delete_menus','menus','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(10,'browse_pages','pages','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(11,'read_pages','pages','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(12,'edit_pages','pages','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(13,'add_pages','pages','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(14,'delete_pages','pages','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(15,'browse_roles','roles','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(16,'read_roles','roles','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(17,'edit_roles','roles','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(18,'add_roles','roles','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(19,'delete_roles','roles','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(20,'browse_users','users','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(21,'read_users','users','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(22,'edit_users','users','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(23,'add_users','users','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(24,'delete_users','users','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(25,'browse_posts','posts','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(26,'read_posts','posts','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(27,'edit_posts','posts','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(28,'add_posts','posts','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(29,'delete_posts','posts','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(30,'browse_categories','categories','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(31,'read_categories','categories','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(32,'edit_categories','categories','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(33,'add_categories','categories','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(34,'delete_categories','categories','2017-09-05 10:11:44','2017-09-05 10:11:44',NULL),
	(40,'browse_blocks','blocks','2017-09-05 10:26:25','2017-09-05 10:26:25',NULL),
	(41,'read_blocks','blocks','2017-09-05 10:26:25','2017-09-05 10:26:25',NULL),
	(42,'edit_blocks','blocks','2017-09-05 10:26:25','2017-09-05 10:26:25',NULL),
	(43,'add_blocks','blocks','2017-09-05 10:26:25','2017-09-05 10:26:25',NULL),
	(44,'delete_blocks','blocks','2017-09-05 10:26:25','2017-09-05 10:26:25',NULL),
	(45,'browse_bookings','bookings','2017-09-05 10:35:10','2017-09-05 10:35:10',NULL),
	(46,'read_bookings','bookings','2017-09-05 10:35:10','2017-09-05 10:35:10',NULL),
	(47,'edit_bookings','bookings','2017-09-05 10:35:10','2017-09-05 10:35:10',NULL),
	(48,'add_bookings','bookings','2017-09-05 10:35:10','2017-09-05 10:35:10',NULL),
	(49,'delete_bookings','bookings','2017-09-05 10:35:10','2017-09-05 10:35:10',NULL),
	(50,'browse_rooms','rooms','2017-09-06 07:56:00','2017-09-06 07:56:00',NULL),
	(51,'read_rooms','rooms','2017-09-06 07:56:00','2017-09-06 07:56:00',NULL),
	(52,'edit_rooms','rooms','2017-09-06 07:56:00','2017-09-06 07:56:00',NULL),
	(53,'add_rooms','rooms','2017-09-06 07:56:00','2017-09-06 07:56:00',NULL),
	(54,'delete_rooms','rooms','2017-09-06 07:56:00','2017-09-06 07:56:00',NULL),
	(55,'browse_siteplans','siteplans','2017-09-06 08:10:06','2017-09-06 08:10:06',NULL),
	(56,'read_siteplans','siteplans','2017-09-06 08:10:06','2017-09-06 08:10:06',NULL),
	(57,'edit_siteplans','siteplans','2017-09-06 08:10:06','2017-09-06 08:10:06',NULL),
	(58,'add_siteplans','siteplans','2017-09-06 08:10:06','2017-09-06 08:10:06',NULL),
	(59,'delete_siteplans','siteplans','2017-09-06 08:10:06','2017-09-06 08:10:06',NULL),
	(75,'browse_facilities','facilities','2017-09-06 08:56:55','2017-09-06 08:56:55',NULL),
	(76,'read_facilities','facilities','2017-09-06 08:56:55','2017-09-06 08:56:55',NULL),
	(77,'edit_facilities','facilities','2017-09-06 08:56:55','2017-09-06 08:56:55',NULL),
	(78,'add_facilities','facilities','2017-09-06 08:56:55','2017-09-06 08:56:55',NULL),
	(79,'delete_facilities','facilities','2017-09-06 08:56:55','2017-09-06 08:56:55',NULL),
	(80,'browse_contacts','contacts','2017-09-06 10:25:49','2017-09-06 10:25:49',NULL),
	(81,'read_contacts','contacts','2017-09-06 10:25:49','2017-09-06 10:25:49',NULL),
	(82,'edit_contacts','contacts','2017-09-06 10:25:49','2017-09-06 10:25:49',NULL),
	(83,'add_contacts','contacts','2017-09-06 10:25:49','2017-09-06 10:25:49',NULL),
	(84,'delete_contacts','contacts','2017-09-06 10:25:49','2017-09-06 10:25:49',NULL);

/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`)
VALUES
	(1,0,NULL,'Lorem Ipsum Post',NULL,'This is the excerpt for the Lorem Ipsum Post','<p>This is the body of the lorem ipsum post</p>','posts/nlje9NZQ7bTMYOUG4lF1.jpg','lorem-ipsum-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(2,0,NULL,'My Sample Post',NULL,'This is the excerpt for the sample Post','<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>','posts/7uelXHi85YOfZKsoS6Tq.jpg','my-sample-post','Meta Description for sample post','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(3,0,NULL,'Latest Post',NULL,'This is the excerpt for the latest post','<p>This is the body for the latest post</p>','posts/9txUSY6wb7LTBSbDPrD9.jpg','latest-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(4,0,NULL,'Yarr Post',NULL,'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.','<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>','posts/yuk1fBwmKKZdY2qR1ZKM.jpg','yarr-post','this be a meta descript','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-09-05 10:11:44','2017-09-05 10:11:44');

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`)
VALUES
	(1,'admin','Administrator','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(3,'webmin','Web Admin','2017-09-06 07:33:00','2017-09-06 07:33:12');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rooms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rooms`;

CREATE TABLE `rooms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `facilities` text,
  `image` varchar(255) DEFAULT NULL,
  `available` int(11) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `layout` enum('deluxe','penthouse','studio','twobedroom') DEFAULT NULL,
  `night` varchar(255) DEFAULT NULL,
  `month` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;

INSERT INTO `rooms` (`id`, `name`, `facilities`, `image`, `available`, `area`, `layout`, `night`, `month`, `created_at`, `updated_at`)
VALUES
	(1,'Studio','<ul>\r\n<li>Espresso machine</li>\r\n<li>Strong WiFi&nbsp;</li>\r\n<li>Handmade bed</li>\r\n<li>Minimalism furniture</li>\r\n<li>Luxurious bathroom</li>\r\n<li>Flatscreen Tv</li>\r\n<li>Fancy kitchen</li>\r\n</ul>\r\n<p>&nbsp;</p>','rooms/September2017/MBiGj6yFUAbHXVmy6Wk5.jpg',20,'25','studio','Rp. 450.000','Rp. 11.000.000','2016-05-24 19:18:00','2017-09-08 07:08:41'),
	(2,'Penthouse','<ul>\r\n<li>Espresso machine</li>\r\n<li>Strong WiFi&nbsp;</li>\r\n<li>Handmade bed</li>\r\n<li>Minimalism furniture</li>\r\n<li>Luxurious bathroom</li>\r\n<li>Flatscreen Tv</li>\r\n<li>Fancy kitchen</li>\r\n</ul>\r\n<p>&nbsp;</p>','rooms/September2017/arMLpFymrgpEm8cQlmPe.jpg',20,'25','penthouse','Rp. 450.000','Rp. 11.000.000','2016-05-24 19:18:00','2017-09-08 07:08:26'),
	(3,'Deluxe Studio','<ul>\r\n<li>Espresso machine</li>\r\n<li>Strong WiFi&nbsp;</li>\r\n<li>Handmade bed</li>\r\n<li>Minimalism furniture</li>\r\n<li>Luxurious bathroom</li>\r\n<li>Flatscreen Tv</li>\r\n<li>Fancy kitchen</li>\r\n</ul>\r\n<p>&nbsp;</p>','rooms/September2017/ejlAT8QYppBQnQigRMZ1.jpg',20,'25','deluxe','Rp. 450.000','Rp. 11.000.000','2016-05-24 20:30:00','2017-09-08 07:08:10'),
	(4,'Two Bedroom','<ul>\r\n<li>Espresso machine</li>\r\n<li>Strong WiFi&nbsp;</li>\r\n<li>Handmade bed</li>\r\n<li>Minimalism furniture</li>\r\n<li>Luxurious bathroom</li>\r\n<li>Flatscreen Tv</li>\r\n<li>Fancy kitchen</li>\r\n</ul>','rooms/September2017/KbN20ALnTNrDp4uDJGfv.jpg',20,'25','twobedroom','Rp. 450.000','Rp. 11.000.000','2017-09-08 14:07:31','2017-09-08 07:07:31');

/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`)
VALUES
	(1,'title','Site Title','Nostoi','','text',1),
	(2,'description','Site Description','Art Space For Living','','text',2),
	(3,'logo','Site Logo','settings/September2017/TDryiHk1uZ5Xw7PQyVHi.png','','image',3),
	(4,'admin_bg_image','Admin Background Image','','','image',9),
	(5,'admin_title','Admin Title','Nostoi','','text',4),
	(6,'admin_description','Admin Description','Welcome to Nostoi Dashboard.','','text',5),
	(7,'admin_loader','Admin Loader','','','image',6),
	(8,'admin_icon_image','Admin Icon Image','','','image',7),
	(9,'google_analytics_client_id','Google Analytics Client ID','UA-106392716-1','','text',9);

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table siteplans
# ------------------------------------------------------------

DROP TABLE IF EXISTS `siteplans`;

CREATE TABLE `siteplans` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `siteplan` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `pdf` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `siteplans` WRITE;
/*!40000 ALTER TABLE `siteplans` DISABLE KEYS */;

INSERT INTO `siteplans` (`id`, `room_id`, `name`, `siteplan`, `photo`, `pdf`, `created_at`, `updated_at`)
VALUES
	(1,1,'Siteplan 1 ','siteplans/September2017/1kd3tjVxEi2w04xlIjOU.png','siteplans/September2017/jVtCiOADQD180IPZDYJJ.jpg','siteplans/September2017/CVZQfIHDn7IeMwdpyASv.pdf','2017-09-08 14:42:27','2016-05-22 23:52:19'),
	(2,1,'Siteplan 2','siteplans/September2017/1kd3tjVxEi2w04xlIjOU.png','siteplans/September2017/jVtCiOADQD180IPZDYJJ.jpg','siteplans/September2017/CVZQfIHDn7IeMwdpyASv.pdf','2017-09-08 14:42:26','2016-05-22 23:52:19'),
	(3,2,'Siteplan 3','siteplans/September2017/1kd3tjVxEi2w04xlIjOU.png','siteplans/September2017/jVtCiOADQD180IPZDYJJ.jpg','siteplans/September2017/CVZQfIHDn7IeMwdpyASv.pdf','2017-09-08 14:42:25','2017-09-08 07:39:55'),
	(4,3,'Siteplan 4','siteplans/September2017/1kd3tjVxEi2w04xlIjOU.png','siteplans/September2017/jVtCiOADQD180IPZDYJJ.jpg','siteplans/September2017/CVZQfIHDn7IeMwdpyASv.pdf','2017-09-08 14:42:24','2016-05-22 23:52:19'),
	(5,3,'Siteplan 5','siteplans/September2017/1kd3tjVxEi2w04xlIjOU.png','siteplans/September2017/jVtCiOADQD180IPZDYJJ.jpg','siteplans/September2017/CVZQfIHDn7IeMwdpyASv.pdf','2017-09-08 14:42:24','2016-05-22 23:52:19'),
	(6,3,'Siteplan 6','siteplans/September2017/1kd3tjVxEi2w04xlIjOU.png','siteplans/September2017/jVtCiOADQD180IPZDYJJ.jpg','siteplans/September2017/CVZQfIHDn7IeMwdpyASv.pdf','2017-09-08 14:42:23','2016-05-22 23:52:19'),
	(7,4,'Siteplan 7','siteplans/September2017/1kd3tjVxEi2w04xlIjOU.png','siteplans/September2017/B0V0kSVOY8o1hkveJeU1.jpg','siteplans/September2017/CVZQfIHDn7IeMwdpyASv.pdf','2017-09-08 14:42:18','2017-09-08 07:32:55');

/*!40000 ALTER TABLE `siteplans` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sliders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sliders`;

CREATE TABLE `sliders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table translations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `translations`;

CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`)
VALUES
	(1,'data_types','display_name_singular',1,'pt','Post','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(2,'data_types','display_name_singular',2,'pt','Página','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(3,'data_types','display_name_singular',3,'pt','Utilizador','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(4,'data_types','display_name_singular',4,'pt','Categoria','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(5,'data_types','display_name_singular',5,'pt','Menu','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(6,'data_types','display_name_singular',6,'pt','Função','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(7,'data_types','display_name_plural',1,'pt','Posts','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(8,'data_types','display_name_plural',2,'pt','Páginas','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(9,'data_types','display_name_plural',3,'pt','Utilizadores','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(10,'data_types','display_name_plural',4,'pt','Categorias','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(11,'data_types','display_name_plural',5,'pt','Menus','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(12,'data_types','display_name_plural',6,'pt','Funções','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(13,'pages','title',1,'pt','Olá Mundo','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(14,'pages','slug',1,'pt','ola-mundo','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(15,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(16,'menu_items','title',1,'pt','Painel de Controle','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(17,'menu_items','title',2,'pt','Media','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(18,'menu_items','title',3,'pt','Publicações','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(19,'menu_items','title',4,'pt','Utilizadores','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(20,'menu_items','title',5,'pt','Categorias','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(21,'menu_items','title',6,'pt','Páginas','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(22,'menu_items','title',7,'pt','Funções','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(23,'menu_items','title',8,'pt','Ferramentas','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(24,'menu_items','title',9,'pt','Menus','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(25,'menu_items','title',10,'pt','Base de dados','2017-09-05 10:11:44','2017-09-05 10:11:44'),
	(26,'menu_items','title',11,'pt','Configurações','2017-09-05 10:11:44','2017-09-05 10:11:44');

/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,1,'Admin','yosua.reinaldo@gmail.com','users/default.png','$2y$10$zH/6zjWNlmya2mO47kC5nOj32QYaOta9J78oxyQhU9E7bsL4sdHWO','kEAKbaruiYXJrH7w8EVECrfssP6XEuBynfNZz4Qy4MI4R5jYxCMXfp72cLke','2017-09-05 10:11:44','2017-09-06 07:33:36'),
	(2,3,'Nostoi','nostoiid@gmail.com','users/default.png','$2y$10$1u642rzoEqxT9pdpOubu1uSP/SpLHNDTunpie0G7zC5SR7/eVZedq','TszU6xhjwBa4wpFDktGy0WDGY9S202pWSmBU6E1RzqEiHbH7BLzykgq8hjWF','2017-09-06 07:34:06','2017-09-06 07:47:14');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
